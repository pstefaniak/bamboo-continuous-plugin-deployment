package com.atlassian.bamboo.plugins.confdeploy;

import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.plan.artifact.ArtifactDefinition;
import com.atlassian.bamboo.plan.artifact.ArtifactSubscriptionManager;
import com.atlassian.bamboo.plan.cache.ImmutableJob;
import com.atlassian.bamboo.task.AbstractTaskConfigurator;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.utils.Pair;
import com.atlassian.bamboo.utils.error.ErrorCollection;
import com.atlassian.plugin.util.ContextClassLoaderSwitchingUtil;
import com.atlassian.sal.api.component.ComponentLocator;
import com.google.common.base.Function;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.opensymphony.xwork.TextProvider;
import org.apache.commons.lang.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Implements the logic for editing and configuring the task.
 */
public class AutoDeployConfigurator extends AbstractTaskConfigurator
{
    private static final Set<String> FIELDS_TO_COPY = Sets.newHashSet(
            AutoDeployTask.PLUGIN_ARTIFACT,
            AutoDeployTask.CONFLUENCE_BASE_URL,
            AutoDeployTask.CONFLUENCE_ADMIN_USER
    );

    /**
     * Converts the user's selected configuration into a persistable String/String map - this is the configuration map
     * that will be passed into the task when it executes.
     * <p/>
     * The {@link AutoDeployConfigurator#validate(com.atlassian.bamboo.collections.ActionParametersMap, com.atlassian.bamboo.utils.error.ErrorCollection)}
     * method is called, and must have succeeded, before this method is called.
     *
     * @param params                 The parameters entered in by the user.
     * @param previousTaskDefinition
     * @return
     */
    @NotNull
    @Override
    public Map<String, String> generateTaskConfigMap(@NotNull final ActionParametersMap params, @Nullable final TaskDefinition previousTaskDefinition)
    {
        final Map<String, String> config = super.generateTaskConfigMap(params, previousTaskDefinition);
        config.put(AutoDeployTask.CONFLUENCE_BASE_URL, params.getString(AutoDeployTask.CONFLUENCE_BASE_URL));
        config.put(AutoDeployTask.CONFLUENCE_ADMIN_USER, params.getString(AutoDeployTask.CONFLUENCE_ADMIN_USER));

        // Encrypt the password before persisting
        final String rawPassword = params.getString(AutoDeployTask.CONFLUENCE_ADMIN_PASSWORD);
        final Pair<String, String> encryptedPassword = Crypto.encrypt(rawPassword);
        config.put(AutoDeployTask.CONFLUENCE_KEY, encryptedPassword.getFirst());
        config.put(AutoDeployTask.CONFLUENCE_ADMIN_PASSWORD, encryptedPassword.getSecond());

        // TODO: It would be better to store the unique ID of the artifact, but not sure how to get it from the WebWork front-end, just yet.
        // Store the name instead.
        config.put(AutoDeployTask.PLUGIN_ARTIFACT, params.getString(AutoDeployTask.PLUGIN_ARTIFACT));
        return config;
    }

    /**
     * Populates the default configuration for a new instance of the task (ie. this is where you should populate the
     * {@param context} with sensible default values.... I think?
     */
    @Override
    public void populateContextForCreate(@NotNull final Map<String, Object> context)
    {
        super.populateContextForCreate(context);

        // Get all available subscribed artifacts from the parent Job
        if (!context.containsKey("plan"))
            return;

        ImmutableJob parent = (ImmutableJob) context.get("plan");
        ArtifactSubscriptionManager artifactSubscriptionManager = ServiceAccessor.getArtifactSubscriptionManager();
        List<String> artifacts = Lists.transform(artifactSubscriptionManager.findSubscriptionsPossibleForPlan(parent),
                new Function<ArtifactDefinition, String>()
                {
                    @Override
                    public String apply(ArtifactDefinition artifactDefinition)
                    {
                        return new AvailableArtifact(artifactDefinition).toString();
                    }
                });
        context.put("artifacts", artifacts);

        if (artifacts.size() > 0)
            context.put(AutoDeployTask.PLUGIN_ARTIFACT, artifacts.get(0)); // select a default

        context.put(AutoDeployTask.CONFLUENCE_BASE_URL, "eg. 'http://localhost:1990/confluence'");
    }

    /**
     * Populates the configuration with its previously persisted config (ie. this is where you implement re-loading a
     * saved configuration).
     */
    @Override
    public void populateContextForEdit(@NotNull final Map<String, Object> context, @NotNull final TaskDefinition taskDefinition)
    {
        super.populateContextForEdit(context, taskDefinition);

        // Get all available subscribed artifacts from the parent Job
        if (!context.containsKey("plan"))
            return;

        ImmutableJob parent = (ImmutableJob) context.get("plan");
        ArtifactSubscriptionManager artifactSubscriptionManager = ServiceAccessor.getArtifactSubscriptionManager();
        List<String> artifacts = Lists.transform(artifactSubscriptionManager.findSubscriptionsPossibleForPlan(parent),
                new Function<ArtifactDefinition, String>()
                {
                    @Override
                    public String apply(ArtifactDefinition artifactDefinition)
                    {
                        return new AvailableArtifact(artifactDefinition).toString();
                    }
                });
        context.put("artifacts", artifacts);
        taskConfiguratorHelper.populateContextWithConfiguration(context, taskDefinition, FIELDS_TO_COPY);
    }

    /**
     * Populates the configuration in a read-only context.
     *
     * @param context
     * @param taskDefinition
     */
    @Override
    public void populateContextForView(@NotNull final Map<String, Object> context, @NotNull final TaskDefinition taskDefinition)
    {
        super.populateContextForView(context, taskDefinition);
        taskConfiguratorHelper.populateContextWithConfiguration(context, taskDefinition, FIELDS_TO_COPY);
    }

    /**
     * Called when the configuration is saved by the user - ensures that the selected configuration is valid. Add new errors
     * if it is not.
     */
    @Override
    public void validate(@NotNull final ActionParametersMap params, @NotNull final ErrorCollection errorCollection)
    {
        super.validate(params, errorCollection);

        // this is all a shitty workaround for https://jira.atlassian.com/browse/BAM-10549
        ContextClassLoaderSwitchingUtil.runInContext(getClass().getClassLoader(), new Runnable()
        {
            @Override
            public void run()
            {
                TextProvider textProvider = ComponentLocator.getComponent(TextProvider.class);
                final String baseURL = params.getString(AutoDeployTask.CONFLUENCE_BASE_URL);
                if (StringUtils.isBlank(baseURL))
                {
                    errorCollection.addError(AutoDeployTask.CONFLUENCE_BASE_URL, textProvider.getText("confdeploy.url.error"));
                }

                final String username = params.getString(AutoDeployTask.CONFLUENCE_ADMIN_USER);
                if (StringUtils.isBlank(username))
                {
                    errorCollection.addError(AutoDeployTask.CONFLUENCE_ADMIN_USER, textProvider.getText("confdeploy.username.error"));
                }

                final String password = params.getString(AutoDeployTask.CONFLUENCE_ADMIN_PASSWORD);
                if (StringUtils.isBlank(password))
                {
                    errorCollection.addError(AutoDeployTask.CONFLUENCE_ADMIN_PASSWORD, textProvider.getText("confdeploy.password.error"));
                }


                final String artifactName = params.getString(AutoDeployTask.PLUGIN_ARTIFACT);
                if (StringUtils.isBlank(artifactName))
                {
                    // TODO: Ensure artifact is a single jar file
                    errorCollection.addError(AutoDeployTask.PLUGIN_ARTIFACT, textProvider.getText("confdeploy.jar.error"));
                }
            }
        });


    }
}
