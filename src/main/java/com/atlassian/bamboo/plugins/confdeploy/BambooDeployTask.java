package com.atlassian.bamboo.plugins.confdeploy;

public class BambooDeployTask extends AutoDeployTask
{
    @Override
    public Product getTargetProduct()
    {
        return Product.BAMBOO;
    }
}
