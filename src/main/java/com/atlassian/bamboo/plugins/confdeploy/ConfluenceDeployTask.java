package com.atlassian.bamboo.plugins.confdeploy;

/**
 *
 */
public class ConfluenceDeployTask extends AutoDeployTask
{
    @Override
    public Product getTargetProduct()
    {
        return Product.CONFLUENCE;
    }
}
