package com.atlassian.bamboo.plugins.confdeploy;

/**
 *
 */
public class JiraDeployTask extends AutoDeployTask
{
    @Override
    public Product getTargetProduct()
    {
        return Product.JIRA;
    }
}
