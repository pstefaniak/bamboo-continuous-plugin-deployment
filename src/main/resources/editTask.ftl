[#-- ftlvariable name="context" type="java.util.Map<String, Object." --]

[@ww.select cssClass="builderSelectWidget" labelKey="confdeploy.jar" name="confDeployJar" list=context.get("artifacts") required="true" /]
[@ww.textfield cssClass="long-field" labelKey="confdeploy.url" name="confDeployURL" required="true" /]
[@ww.textfield cssClass="long-field" labelKey="confdeploy.username" name="confDeployUsername" required="true"/]
[@ww.password cssClass="long-field" labelKey="confdeploy.password" name="confDeployPassword" required="true" /]




